import { LightningElement, track } from 'lwc';

export default class UploadCsvPOC extends LightningElement {
  @track txtFile
  myFunction(evt) {
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
    for (let i = 0, f; f = files[i]; i++) {
      let reader = new FileReader();
      reader.onload = function (event) {
        // NOTE: event.target point to FileReader
        let contents = event.target.result;
        let lines = contents.split('\n');
        console.log(lines)
        //////
      };
      reader.readAsText(f);
    }
  }

  convertToJson(csv) {
    var lines = csv.split("\n");
    var result = [];
    var headers = lines[0].split(",");
    for (let i = 1; i < lines.length; i++) {
      let obj = {};
      let currentline = lines[i].split(",");
      for (let j = 0; j < headers.length; j++) {
        obj[headers[j]] = currentline[j];
      }
      result.push(obj);
    }
    //return result; //JavaScript object
    return JSON.stringify(result); //JSON
  }


}