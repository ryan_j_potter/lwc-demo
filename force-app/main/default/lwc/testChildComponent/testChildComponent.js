import { LightningElement, api, track, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';


export default class TestChildComponent extends LightningElement {
    @api record;
}