import { LightningElement, track, api, wire } from 'lwc';
import ACCOUNT_OBJECT from '@salesforce/schema/Account';
import ACCOUNT_NAME_FIELD from '@salesforce/schema/Account.Name';
import { getRecord } from 'lightning/uiRecordApi';
import getChildren from '@salesforce/apex/lwcWireTest.getChildren';
import fetchDataHelper from 'c/fetchDataHelper';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'


const columns = [
    { label: 'Label', fieldName: 'name' },
    { label: 'Website', fieldName: 'website', type: 'url' },
    { label: 'Phone', fieldName: 'phone', type: 'phone' },
    { label: 'Balance', fieldName: 'amount', type: 'currency' },
    { label: 'CloseAt', fieldName: 'closeAt', type: 'date' },
];



export default class LightningExampleAccordionMultiple extends LightningElement {
    @track activeSections = ['A', 'C'];
    @track activeSectionsMessage = '';
    @track recordString;
    @track dynamicContent = 'Hellow';
    @track showSection = false;
    @track showSection2 = false;
    @track childrenList = null;
    @track data = [];
    @track columns = columns;
    @track childId = 'a00B0000009FmKbIAK';
    @track childRecord;
    @api recordId;

    /**
     * Retrieve a list of child records from the server
     * @param {*} recordId the record Id of the current account
     */
    @wire(getChildren, {recordId: "$recordId"})
    children({error, data}) {
        if (data)  {
            console.log("SUCCESS:", JSON.stringify(data))
            this.childrenList = data;
        } else if (error) {
            console.log("Error:", JSON.stringify(error));
        }
    }
   
    /**
     * get the current record data
     * @param recordId the current record Id
     * @param fields the list of fields to retrieve
     */
    @wire(getRecord, { recordId: "$recordId", fields: [ACCOUNT_NAME_FIELD] })
    record({ error, data }) {
        if (data) {
            this.recordString = JSON.stringify(data);
        } else if (error) {
            this.recordString = "ERROR"
        }
    }

    /**
     * get the current record data
     * @param recordId the current record Id
     * @param fields the list of fields to retrieve
     */
    @wire(getRecord, { recordId: "$childId", fields: ["Children_Account__c.Name"] })
    child({ error, data }) {
        if (data) {
            console.log(JSON.stringify(data))
            this.childRecord = data.fields.Name.value;
        } else if (error) {
            console.log("Error:", JSON.stringify(error));
            this.childRecord = "ERROR"
        }
    }


    /**
     * Cotnrol logic for when an accordian section is clicked into
     * @param {*} event the click event details
     */
    handleSectionToggle(event) {
        const openSections = event.detail.openSections;
        if (openSections.length === 0) {
            this.activeSectionsMessage = 'All sections are closed';
        } else {
            this.activeSectionsMessage =
                'Open sections: ' + openSections.join(', ');
        }
    }

    /**
     * Get the table data after the element has been inserted into the DOM
     */
    async connectedCallback() {
        const data = await fetchDataHelper({ amountOfRecords: 100 });
        this.data = data;
        //Display success emssage
        const event = new ShowToastEvent({
            title: 'SUCCESS!',
            message: 'Data succesfully Retrieved',
            variant: "success"
        });
        this.dispatchEvent(event);
    }

    /**
     * handle the clicking of the first button
     * @param {*} event  the click event details
     */
    clicked(event) {
        let contentBlockClasslist = this.template.querySelector(
            '.hiddenSection'
        ).classList;
        contentBlockClasslist.toggle('slds-hidden');
    }

    /**
     * handle the clicking of button 2
     * @param {*} event the click event details
     */
    clicked2(event) {
        this.showSection = this.showSection ? false : true;
    }

    /**
     * Get the list of children records
     */
    get items() {
        return this.childrenList;
    }

    /**
     * Set the child to new target id
     * @param {*} event the click event details
     */
    updateChild(event) {
        console.log(event.target.id);
        this.childId = event.target.id.substring(0, event.target.id.length - 4);
    }
}
