public with sharing class lwcWireTest {
    @AuraEnabled (cacheable=true)
    public static List<Children_Account__c> getChildren(String recordId){
        System.debug(recordId + ' ' + [SELECT Id, Name__c FROM Children_Account__c WHERE Account__c = :recordId]);
      return [SELECT Id, Name__c FROM Children_Account__c WHERE Account__c = :recordId];
    }
}