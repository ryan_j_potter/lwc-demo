declare module "@salesforce/apex/lwcWireTest.getChildren" {
  export default function getChildren(param: {recordId: any}): Promise<any>;
}
